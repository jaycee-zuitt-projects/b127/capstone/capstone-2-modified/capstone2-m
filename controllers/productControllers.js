const Product = require('../models/productModel');


//Creation of product(Admin Only)
module.exports.createProduct = (data) => {
    let newProduct = new Product({
        name: data.product.name,
        desc: data.product.desc,
        img: data.product.img,
        categories: data.product.categories,
        size: data.product.size,
        color: data.product.color,
        price: data.product.price
    })
    return newProduct.save().then((product, error) => {
        if(error){
            return false
        }else{
            return true
        }
    })
}

//retrieval of product
module.exports.getAllProducts = () => {
    return Product.find({}).then(result => {
        return result;
    })
}

//retrieval of single product
module.exports.specificProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}

//modifying/updating product
module.exports.modifyProduct = (reqParams, reqBody) => {
    let updateProduct = {
        name: reqBody.name,
        desc: reqBody.desc,
        img: reqBody.img,
        categories: reqBody.categories,
        size: reqBody.size,
        color: reqBody.color,
        price: reqBody.price
    }
    return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) => {
        if(error){
            return false;
        }else{
            return true;
        }
    })
}


//archive of product
module.exports.archiveProduct = (reqParams) => {
    let tempDelete = {
        isActive: false
    }
    return Product.findByIdAndUpdate(reqParams.productId, tempDelete).then((product, error)=> {
        if(error){
            return false;
        }else{
            return true;
        }
    })
}

//get all active products

module.exports.getActiveProducts = () => {
    return Product.find({isActive: true}).then(result => {
        return result;
    })
}