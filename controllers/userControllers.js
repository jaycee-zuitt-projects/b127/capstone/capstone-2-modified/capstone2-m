const User = require('../models/userModel');
const Product = require('../models/productModel');
const bcrypt = require('bcryptjs');
const auth = require('../auth');

//registration of users
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        address: reqBody.address,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
    })
    return newUser.save().then((user, error) => {
        if(error){
            return false;
        }else{
            return(`Account is Successfully Created`)
        }
    })
}

//login users

module.exports.loginUser = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then(result => {
        if(result == null){
            return "User is not yet registered, Please register first or you might input wrong credentials"
        }else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if(isPasswordCorrect){
                return{ accessToken: auth.createAccessToken(result.toObject()) }
            }else{
                return "Wrong password please input correct password";
            }
        }
    })
}

//Admin retrieve all users

module.exports.getAll = () => {
    return User.find({}).then((result, error) => {
        if(error){
            return ("You dont have access on this function");
        }else{
            return result;
        }
    })
}

//Admin get specific


//Assign an Admin(Admin Only)
module.exports.makeAdmin = (reqParams) => {
    let newAdmin = {
        isAdmin: true
    }
    return User.findByIdAndUpdate(reqParams.userId, newAdmin).then((user, error) => {
        if(error){
            return false;
        }else{
            return true;
        }
    })
}


//additional features update user account
module.exports.updateAccount = (reqParams, data) => {
    let update = {
        firstName: data.User.firstName,
        lastName: data.User.lastName,
        email: data.User.email,
        address: data.User.address,
        mobileNo: data.User.mobileNo
    }

    return User.findByIdAndUpdate(reqParams.userId, update).then((user, error) => {
        if(error){
            console.log(error);
            return false;
        }else{
            return true;
        }
    })
}

