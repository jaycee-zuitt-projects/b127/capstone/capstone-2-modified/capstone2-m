//express and cors section(requiring)
const express = require('express');
const cors = require('cors');

//variables for routes
const userRoutes = require('./routers/userRoutes')
const productRoutes = require('./routers/productRoutes');
const orderRoutes = require('./routers/orderRoutes');

//Mongoose section
const mongoose = require('mongoose');
const app = express();

//connect to my MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.z7wiz.mongodb.net/Capstone2?retryWrites=true&w=majority", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
);

//Mongoose connection error handling
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Mongoose Connection Error"));
db.once("open", () => console.log("Mongoose are connected to the cloud database"));

//resources/origin access
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//invoke the routes section
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);


//port 
app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`)
});