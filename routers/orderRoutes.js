const Order = require("../models/orderModel");
const auth = require('../auth');
const orderController = require('../controllers/orderControllers');

const router = require("express").Router();

//CREATE

router.post("/", auth.verify, (req, res) => {
  const data = {
    userId: auth.decode(req.headers.authorization).id,
    Order: req.body
  }
  orderController.order(data).then(result => res.send(result));
})

//Authenticated User retrieval orders

router.get("/myOrders", auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}
	console.log(data.userId)
	if (!data.isAdmin) {
		orderController.myOrders(data).then(result => res.send(result))
	} else {
		res.send(false);
	}
})

//get all orders admin only
router.get("/allOrders", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }
  if(data.isAdmin){
    return orderController.allOrders(data).then(result => res.send(result))
  }else{
    console.log(error)
    res.send(false);
  }
})





module.exports = router;