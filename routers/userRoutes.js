const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');

//registration of users
router.post('/register', (req, res) => {
    userController.registerUser(req.body).then((result, error) => {
        if(error){
            console.log(error);
            return false;
        }else{
            res.send(result);
        }
    })
})

//login users
router.post('/login', (req, res) => {
    userController.loginUser(req.body).then((result, error) => {
        if(error){
            console.log(error);
            return false;
        }else{
            res.send(result);
        }
    })
})

//Admin retrieve all users
router.get('/getAll', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    if(data.isAdmin){
        userController.getAll().then(result => res.send(result))
    }else{
        return ("You dont have access on this function");
    }
})

//Admin get specific


//Assign an Admin(Admin Only)
router.put('/:userId/makeAdmin', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin){
        userController.makeAdmin(req.params).then(result => res.send(result))
    }else{
        return false
    }
})

//additional features update user account
router.put("/:userId", auth.verify, (req, res) => {
    const data = {
        userId: auth.decode(req.headers.authorization).id,
        User: req.body
    }
    userController.updateAccount(req.params, data).then(result => res.send(result))
})




module.exports = router;